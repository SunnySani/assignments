package assignments.assignment1;

import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    public static String encode(String data) {
        int dataLength; // Panjangnya data setelah ditambahkan parity bit
        int position; // Posisi menghitung 1 dalam data
        int distance; // Jarak yang dideterminasi oleh masing-masing parity bit
        int param; // Variabel untuk menghitung banyaknya 1 dalam data
        int x = 0; // Variabel yang menampung kuantitas parity bit
        boolean determiner; // Parameter untuk do-while loop

        // Membuat data menjadi lebih panjang karena akan ditambah parity bit
        // Sesuai dengan posisinya dan kuantitasnya (parity bit)
        do {
            distance = (int) Math.pow(2, x);
            if (distance <= data.length()) {
                data = data.substring(0, distance - 1) + "X" + data.substring(distance - 1);
                x++;
            } else {
                x--; // Agar x mempresentasikan jumlah parity bit. Hitungan dimulai dari 0
                break;
            }
        } while (true);

        // Loop berdasarkan kuantitas parity bit dimulai dari parity but terbesar
        for (int i = x; i >= 0; i--) {
            dataLength = data.length();
            distance = (int) Math.pow(2, i);
            position = (int) Math.pow(2, i);
            param = 0;
            determiner = false; // Parameter loop dibawah

            // Loop untuk menghitung jumlah angka 1 berdasarkan posisi setelah parity bit
            do {
                for (int j = 0; j < distance; j++) {

                    // Jika indeks data yang dicek melebihi indeks dalam string
                    if (position + j > dataLength) {
                        determiner = true; // menghentikan do-while loop
                        break; // menghentikan for loop
                        // Jika indeks data yang dicek sama dengan indeks dalam string
                    } else if (position + j == dataLength) {
                        if (data.substring(position + j - 1).equals("1")) {
                            param += 1;
                        } else {
                            ; // param += 0;
                        }
                        // Jika indeks data yang dicek kurang dari indeks dalam string
                    } else {
                        if (data.substring(position + j - 1, position + j).equals("1")) {
                            param += 1;
                        } else {
                            ; // param += 0;
                        }
                    }
                }
                position = position + distance * 2; // Meneruskan posisi pengecekan parity bit
            } while (!determiner);

            // If statement untuk menambahkan parity bit berdasarkan ganjil/genapnya
            // jumlah angka 1
            if (param % 2 == 0) {
                data = data.substring(0, distance - 1) + "0" + data.substring(distance);
            } else {
                data = data.substring(0, distance - 1) + "1" + data.substring(distance);
            }
        }

        return data;
    }

    public static String decode(String code) {
        int parCodeCount = 0; // Variabel untuk menampung kuantitas parity bit
        final int dataLength = code.length(); // Panjangnya data
        int temp = dataLength; // Variabel yang digunakan untuk mencari kuantitas parity bit
        int parPosition = 0; // Posisi parity bit
        int distance; // Jarak yang dideterminasi oleh masing-masing parity bit
        int position; // Posisi menghitung 1 dalam data
        int wrongCode = 0; // Menghitung adakah kesalahan pada parity but ke-n
        int param = 0; // Variabel untuk menghitung banyaknnya 1 dalam data
        boolean determiner; // parameter untuk do-while loop

        // while loop untuk mencari kuantitas parity bit yang ada
        while (temp != 0) {
            parCodeCount++;
            temp = temp / 2;
        }

        // for loop untuk menentukan kebenaran dari bit yang ada berdasarkan parity bit
        // terkanan
        for (int i = parCodeCount; i > 0; i--) {
            parPosition = (int) Math.pow(2, i - 1) - 1;
            distance = (int) Math.pow(2, i - 1);
            position = (int) Math.pow(2, i - 1);
            param = 0;
            determiner = false;
            do {
                for (int j = 0; j < distance; j++) {
                    // Jika indeks data melebihi indeks dalam string
                    if (position + j > dataLength) {
                        determiner = true; // menghentikan do-while loop
                        break; // keluar dari for loop
                        // Jika indeks data yang dicek sama dengan indeks dalam string
                    } else if (position + j == dataLength) {
                        if (code.substring(position + j - 1).equals("1")) {
                            param += 1;
                        } else {
                            ; // param += 0;
                        }
                        // Jika indeks data yang dicek kurang dari indeks dalam string
                    } else { // position + j + 1 < data length
                        if (code.substring(position + j - 1, position + j).equals("1")) {
                            param += 1;
                        } else {
                            ; // param += 0;
                        }
                    }
                }
                position = position + distance * 2; // Meneruskan posisi pengecekan parity bit
            } while (!determiner);

            // If else untuk menentukan apakah param(jumlah angka 1 dalam data yang
            // dihitung)
            // genap berarti data benar, tidak ada perubahan
            // ganjil berarti data salah, ada perubahan
            // posisi parity yang salah dengan indeks awal adalah 1.
            // wrongCode akan terus ditambahkan dengan parity bit yang terdapat kesalahan
            if (param % 2 == 0) {
                ; // pass
            } else {
                wrongCode += parPosition + 1;
            }
        }

        // Jika wrongCode = 0 (data benar). Maka lanjutkan.
        // Jika wrongCode = 1 (data ada yang salah), maka data pada indeks ke wrongCode
        // dengan indeks awal adalah 1 diubah.
        if (wrongCode == 0) {
            ; // pass
        } else {
            if (code.substring(wrongCode - 1, wrongCode).equals("1")) {
                code = code.substring(0, wrongCode - 1) + "0" + code.substring(wrongCode);
            } else {
                code = code.substring(0, wrongCode - 1) + "1" + code.substring(wrongCode);
            }
        }

        // for-loop untuk menghapus parity bit
        for (int i = parCodeCount; i > 0; i--) {
            parPosition = (int) Math.pow(2, i - 1) - 1;
            code = code.substring(0, parPosition) + code.substring(parPosition + 1);
        }

        return code;
    }

    /**
     * Main program for Hamming Code.
     * 
     * @param args unused
     */
    public static void main(final String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        final Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            final int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                final String data = in.next();
                final String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                final String code = in.next();
                final String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}