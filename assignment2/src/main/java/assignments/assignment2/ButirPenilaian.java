package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    // Assign nilai, jika nilai lebih dari 0, maka nilai dalam class adalah input
    // Jika nilai kurang dari 0, maka nilai di class = 0. Juga mengassign keterlambatan
    public ButirPenilaian(double nilai, boolean terlambat) {
        if (nilai >= 0)
            this.nilai = nilai;
        else
            this.nilai = 0;
        this.terlambat = terlambat;
        // TODO: buat constructor untuk ButirPenilaian.
    }

    // Mengembalikan nilai, jika terlambat dikurang bobot dalam persen
    public double getNilai() {
        // TODO: kembalikan nilai yang sudah disesuaikan dengan keterlambatan.
        if (this.terlambat == true)
            return this.nilai * (100 - PENALTI_KETERLAMBATAN) / 100;
        else
            return this.nilai;
    }

    // Menambahkan method toString, jika terlambat ditandai (T)
    @Override
    public String toString() {
        // TODO: kembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
        if (this.terlambat == true) {
            return String.format("%.2f (T)", this.getNilai());
        } else
            return String.format("%.2f", this.getNilai());
    }
}
