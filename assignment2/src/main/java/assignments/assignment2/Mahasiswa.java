package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    // Constructor untuk menyimpan objek nama, npm, dan komponenPenilaian
    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian; //ERROR(?)
    }

    //Mengembalikan komponenPenilaian berdasarkan nama yang dimiliki mahasiswa
    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for (KomponenPenilaian item: komponenPenilaian)
            if (item.getNama().equals(namaKomponen))
                return item;
        return null;
    }

    // Mengembalikan npm mahasiswa
    public String getNpm() {
        // TODO: kembalikan NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    // Mereturn string darire432`
    public String rekap() {
        // TODO: kembalikan rekapan sesuai dengan permintaan soal.
        String rekap = "";
        double nilaiAkhir = 0;
        for (KomponenPenilaian item: this.komponenPenilaian) {
            rekap += item.toString() + "\n";
            nilaiAkhir += item.getNilai();
        }
        rekap += String.format("Nilai akhir: %.2f\n", nilaiAkhir);
        rekap += String.format("Huruf: %s\n", Mahasiswa.getHuruf(nilaiAkhir));
        rekap += String.format("%s\n", Mahasiswa.getKelulusan(nilaiAkhir));
        return rekap;
    }

    // Method toString untuk Mahasiswa
    @Override
    public String toString() {
        // TODO: kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return this.npm + " - " + this.nama;
    }

    // Method untuk mendapatkan detail dengan memanfaatkan method dari Mahasiswa
    // dan KomponenPenilaian
    public String getDetail() {
        // TODO: kembalikan detail dari Mahasiswa sesuai permintaan soal.
        String detail = "";
        double nilaiAkhir = 0;
        for (KomponenPenilaian item: this.komponenPenilaian) {
            detail += item.getDetail() + "\n";
            nilaiAkhir += item.getNilai();
        }
        detail += String.format("Nilai akhir: %.2f\n", nilaiAkhir);
        detail += String.format("Huruf: %.2s\n", Mahasiswa.getHuruf(nilaiAkhir));
        detail += String.format("%s", Mahasiswa.getKelulusan(nilaiAkhir));
        return detail;
    }

    // Digunakan untuk Collection.sort pada AsistenDosen
    @Override
    public int compareTo(Mahasiswa other) {
        // TODO: definisikan cara membandingkan seorang mahasiswa dengan mahasiswa lainnya.
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        return this.npm.compareTo(other.getNpm());
    }
}