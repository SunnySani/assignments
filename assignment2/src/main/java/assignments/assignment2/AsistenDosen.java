package assignments.assignment2;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    // Constructor asisten dosen
    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    // Mengembalikan kode asistendosen
    public String getKode() {
        // TODO: kembalikan kode AsistenDosen.
        return this.kode;
    }

    // Menambahkan input ke list mahasiswa
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        // Fungsi sort menggunakan method compareTo dalam mahasiswa
        Collections.sort(this.mahasiswa);
    }

    //Mereturn mahasiswa dengan npm yang sama
    public Mahasiswa getMahasiswa(String npm) {
        for (Mahasiswa item: mahasiswa)
            if (item.getNpm().equals(npm))
                return item;
        return null;
    }

    // Mereturrn rekap sesuai dengan soal
    public String rekap() {
        String res = "";
        for (int i = 0; i < this.toString().length(); i++)
            res += "~";
        res += "\n\n";
        for (Mahasiswa item: this.mahasiswa)
            res += item.toString() + "\n" + item.rekap();
        return res;
    }

    // Method toString untuk kelas AsistenDosen
    @Override
    public String toString() {
        return String.format("%s - %s", this.kode, this.nama);
    }
}
