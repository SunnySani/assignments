package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot = bobot;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    //Menambahkan nilai ke objek butirPenilaian
    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // TODO: masukkan butir ke butirPenilaian pada index ke-idx.
        this.butirPenilaian[idx] = butir;
    }

    // Mengembalikan nama dari objek KomponenPenilaian
    public String getNama() {
        // TODO: kembalikan nama KomponenPenilaian.
        return this.nama;
    }

    //  Mengembalikan rata-rata dari nilai yang ada di komponenPenilaian
    // null diabaikan
    public double getRerata() {
        // TODO: kembalikan rata-rata butirPenilaian.
        int totalButirPenilaian = 0;
        int jumlah = 0;
        for (ButirPenilaian item: this.butirPenilaian) {
            if (item != null) {
                totalButirPenilaian++;
                jumlah += item.getNilai();
            }
        }
        if (totalButirPenilaian != 0)
            return (double) jumlah / totalButirPenilaian;
        return 0; 
    }

    public double getNilai() {
        // TODO: kembalikan rerata yang sudah dikalikan dengan bobot.
        return this.getRerata() * bobot / 100;
    }

    // Mengembalikan detail dalam bentuk string dengan memanfaatkan
    // method lain dalam ButirPenilaian dan KomponenPenilaian
    public String getDetail() {
        // TODO: kembalikan detail KomponenPenilaian sesuai permintaan soal.
        int count = 1;
        String detail = "~~~ " + this.nama + " (" + this.bobot + "%) ~~~\n" ;
        if (butirPenilaian.length > 1) {
            for (ButirPenilaian item: butirPenilaian) {
            //APAKAH JIKA item dari butir Penilaian == null dihitung?
                if (item != null){
                    if (item.getNilai() != 0)
                        detail += this.nama + " " + count + ": " + item.toString() + "\n";
                }
                count++;
            }
            detail += String.format("Rerata: %.2f\n",this.getRerata());
        } else { // butirPenilaian.length == 1
            if (butirPenilaian[0] != null)
                if (butirPenilaian[0].getNilai() != 0)
                    detail += this.nama + ": " + butirPenilaian[0].toString() + "\n";
        }
        detail += "Kontribusi nilai akhir: " + String.format("%.2f\n", this.getNilai());
        return detail;
    }

    // Method toString
    @Override
    public String toString() {
        // TODO: kembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        double rerata = this.getRerata();
        return String.format("Rerata %s: %.2f", this.getNama(), rerata);
    }

}
