package assignments.assignment3;

public class PeganganTangga extends Benda{
  	
    public PeganganTangga(String name){
        super(name);
    }

    @Override
    public void tambahPersentase() {
        this.setPersentaseMenular(persentaseMenular + 20);
    }

    @Override
    public String toString() {
        return "PEGANGAN TANGGA " + this.getNama();
    }
    
}