package assignments.assignment3;

public class Pintu extends Benda{ 
    
    public Pintu(String name){
        super(name);
    }

    @Override
    public void tambahPersentase() {
        this.setPersentaseMenular(persentaseMenular + 30);
    }

    @Override
    public String toString() {
        return "PINTU " + this.getNama();
    }
}