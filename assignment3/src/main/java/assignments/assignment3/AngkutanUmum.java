package assignments.assignment3;

public class AngkutanUmum extends Benda{
      
    public AngkutanUmum(String name){
        super(name);
    }

    @Override
    public void tambahPersentase() {
        this.setPersentaseMenular(persentaseMenular + 35);
    }

    @Override
    public String toString() {
        return "ANGKUTAN UMUM " + this.getNama();
    }
}