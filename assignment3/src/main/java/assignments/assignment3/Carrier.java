package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    public Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama, String tipe){
        this.nama = nama;
        this.tipe = tipe;
        this.statusCovid = new Negatif();
        this.rantaiPenular = new ArrayList<>();
    }

    public String getNama(){
        return this.nama;
    }

    public String getTipe(){
        return this.tipe;
    }

    public String getStatusCovid(){
        return this.statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        return this.aktifKasusDisebabkan;
    }
    
    public void tambahAktifKasusDisebabkan() {
        this.aktifKasusDisebabkan++;
    }
    
    public void kurangiAktifKasusDisebabkan() {
        this.aktifKasusDisebabkan--;
    }

    public int getTotalKasusDisebabkan(){
        return this.totalKasusDisebabkan;
    }
    
    public void tambahTotalKasusDisebabkan() {
        this.totalKasusDisebabkan++;
    }

    public List<Carrier> getRantaiPenular(){
        return rantaiPenular;
    }

    public void ubahStatus(String status){
        if (status.equals("Negatif")) {
            this.statusCovid = new Negatif();
        }
        else { // status positif
            this.statusCovid = new Positif();
        }
    }

    public void interaksi(Carrier lain){
        if (this.getStatusCovid().equals("Positif") & lain.getStatusCovid().equals("Negatif")) {
            this.statusCovid.tularkan(this, lain);
        }
        else if (this.getStatusCovid().equals("Negatif") & lain.getStatusCovid().equals("Positif")) {
            lain.statusCovid.tularkan(lain, this);
        }
        else;
    }

    @Override
    public abstract String toString();

}
