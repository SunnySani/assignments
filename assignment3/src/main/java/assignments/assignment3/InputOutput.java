package assignments.assignment3;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
    }

    public void setBufferedReader(String inputType) {
        try {
            if (inputType.equalsIgnoreCase("terminal"))
                this.br = new BufferedReader(new InputStreamReader(System.in));
            else
                this.br = new BufferedReader(new FileReader(this.inputFile));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    public void setPrintWriter(String outputType) {
        try {
            if (outputType.equalsIgnoreCase("text"))
                pw = new PrintWriter(this.outputFile);
            else // outputType = terminal
                pw = null;
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public void run() throws IOException {
        world = new World();
        String line;
        String[] lineList;
        Carrier carrier;
        PetugasMedis petugasMedis;
        CleaningService cleaningService;
        String outputRantai;
        if (this.pw != null) {
            while (true) {
                line = br.readLine();
                lineList = line.split(" ");
                // lineList[0] --> command
                // lineList [1,2,3,../] --> data
                if (lineList[0].equalsIgnoreCase("add")) {
                    world.createObject(lineList[1], lineList[2]);
                } else if (lineList[0].equalsIgnoreCase("interaksi")) {
                    world.getCarrier(lineList[1]).interaksi(world.getCarrier(lineList[2]));
                } else if (lineList[0].equalsIgnoreCase("positifkan")) {
                    carrier = world.getCarrier(lineList[1]);
                    carrier.ubahStatus("Positif");
                    carrier.getRantaiPenular().add(carrier);
                } else if (lineList[0].equalsIgnoreCase("sembuhkan")) {
                    petugasMedis = (PetugasMedis) world.getCarrier(lineList[1]);
                    petugasMedis.obati((Manusia) world.getCarrier(lineList[2]));
                } else if (lineList[0].equalsIgnoreCase("bersihkan")) {
                    cleaningService = (CleaningService) world.getCarrier(lineList[1]);
                    cleaningService.bersihkan((Benda) world.getCarrier(lineList[2]));
                }
                else if (lineList[0].equalsIgnoreCase("rantai")) {
                    carrier = world.getCarrier(lineList[1]);
                    try {
                        if (carrier.getStatusCovid().equals("Negatif"))
                            throw new BelumTertularException(carrier.toString() + " berstatus negatif");
                        else { // status covid positif
                            outputRantai = "Rantai Penyebaran " + carrier.toString() + ": ";
                            for (int i = 0; i < carrier.getRantaiPenular().size(); i++) {
                                if (i != carrier.getRantaiPenular().size() - 1)
                                    outputRantai += carrier.getRantaiPenular().get(i).toString() + " -> ";
                                else
                                    outputRantai += carrier.getRantaiPenular().get(i);
                            }
                            pw.println(outputRantai);
                        }
                    } catch (BelumTertularException ex) {
                        pw.println(ex);
                    }
                }
                else if (lineList[0].equalsIgnoreCase("total_kasus_dari_objek")){
                    carrier = world.getCarrier(lineList[1]);
                    pw.println(carrier.toString() + " telah menyebarkan virus "
                            + "COVID " + carrier.getTotalKasusDisebabkan() + " objek");
                }
                else if (lineList[0].equalsIgnoreCase("aktif_kasus_dari_objek")) {
                    carrier = world.getCarrier(lineList[1]);
                    pw.println(carrier.toString() + " telah menyebarkan virus "
                            + "COVID dan masih teridentifikasi positif sebanyak " 
                            + carrier.getAktifKasusDisebabkan() + " objek");
                }
                else if (lineList[0].equalsIgnoreCase("total_sembuh_manusia")) {
                    pw.println("Total sembuh dari kasus COVID yang menimpa ada"
                            + " sebanyak: " + Manusia.getJumlahSembuh() + " kasus");
                }
                else if (lineList[0].equalsIgnoreCase("total_sembuh_petugas_medis")) {
                    petugasMedis = (PetugasMedis) world.getCarrier(lineList[1]);
                    pw.println(petugasMedis.toString() + " menyembuhkan " +
                            petugasMedis.getJumlahDisembuhkan() + " manusia");
                }
                else if (lineList[0].equalsIgnoreCase("total_bersih_cleaning_service")) {
                    cleaningService = (CleaningService) world.getCarrier(lineList[1]);
                    pw.println(cleaningService.toString() + " membersihkan " +
                            cleaningService.getJumlahDibersihkan() + " benda");
                }
                else if (lineList[0].equalsIgnoreCase("exit")) {
                    pw.close();
                    System.exit(0);
                } else;
            }
        } else {
            while (true) {
                line = br.readLine();
                lineList = line.split(" ");
                // lineList[0] --> command
                // lineList [1,2,3,../] --> data
                if (lineList[0].equalsIgnoreCase("add")) {
                    world.createObject(lineList[1], lineList[2]);
                } else if (lineList[0].equalsIgnoreCase("interaksi")) {
                    world.getCarrier(lineList[1]).interaksi(world.getCarrier(lineList[2]));
                } else if (lineList[0].equalsIgnoreCase("positifkan")) {
                    carrier = world.getCarrier(lineList[1]);
                    carrier.ubahStatus("Positif");
                    carrier.getRantaiPenular().add(carrier);
                } else if (lineList[0].equalsIgnoreCase("sembuhkan")) {
                    petugasMedis = (PetugasMedis) world.getCarrier(lineList[1]);
                    petugasMedis.obati((Manusia) world.getCarrier(lineList[2]));
                } else if (lineList[0].equalsIgnoreCase("bersihkan")) {
                    cleaningService = (CleaningService) world.getCarrier(lineList[1]);
                    cleaningService.bersihkan((Benda) world.getCarrier(lineList[2]));
                }
                else if (lineList[0].equalsIgnoreCase("rantai")) {
                    carrier = world.getCarrier(lineList[1]);
                    try {
                        if (carrier.getStatusCovid().equals("Negatif"))
                            throw new BelumTertularException(carrier.toString() + " berstatus negatif");
                        else { // status covid positif
                            outputRantai = "Rantai Penyebaran " + carrier.toString() + ": ";
                            for (int i = 0; i < carrier.getRantaiPenular().size(); i++) {
                                if (i != carrier.getRantaiPenular().size() - 1)
                                    outputRantai += carrier.getRantaiPenular().get(i).toString() + " -> ";
                                else
                                    outputRantai += carrier.getRantaiPenular().get(i);
                            }
                            System.out.println(outputRantai);
                        }
                    } catch (BelumTertularException ex) {
                        System.out.println(ex);
                    }
                }
                else if (lineList[0].equalsIgnoreCase("total_kasus_dari_objek")){
                    carrier = world.getCarrier(lineList[1]);
                    System.out.println(carrier.toString() + " telah menyebarkan virus "
                            + "COVID " + carrier.getTotalKasusDisebabkan() + " objek");
                }
                else if (lineList[0].equalsIgnoreCase("aktif_kasus_dari_objek")) {
                    carrier = world.getCarrier(lineList[1]);
                    System.out.println(carrier.toString() + " telah menyebarkan virus "
                            + "COVID dan masih teridentifikasi positif sebanyak " 
                            + carrier.getAktifKasusDisebabkan() + " objek");
                }
                else if (lineList[0].equalsIgnoreCase("total_sembuh_manusia")) {
                    System.out.println("Total sembuh dari kasus COVID yang menimpa ada"
                            + " sebanyak: " + Manusia.getJumlahSembuh() + " kasus");
                }
                else if (lineList[0].equalsIgnoreCase("total_sembuh_petugas_medis")) {
                    petugasMedis = (PetugasMedis) world.getCarrier(lineList[1]);
                    System.out.println(petugasMedis.toString() + " menyembuhkan " +
                            petugasMedis.getJumlahDisembuhkan() + " manusia");
                }
                else if (lineList[0].equalsIgnoreCase("total_bersih_cleaning_service")) {
                    cleaningService = (CleaningService) world.getCarrier(lineList[1]);
                    System.out.println(cleaningService.toString() + " membersihkan " +
                            cleaningService.getJumlahDibersihkan() + " benda");
                }
                else if (lineList[0].equalsIgnoreCase("exit")) {
                    System.exit(0);
                } else;
            }
        }
    }
    
}