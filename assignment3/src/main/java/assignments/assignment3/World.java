package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        this.listCarrier = new ArrayList<>();
    }

    public Carrier createObject(String tipe, String nama){
        if (getCarrier("nama") == null) {
            if (tipe.equalsIgnoreCase("Ojol"))
                listCarrier.add(new Ojol(nama));
            else if (tipe.equalsIgnoreCase("Jurnalis"))
                listCarrier.add(new Jurnalis(nama));
            else if (tipe.equalsIgnoreCase("Pekerja_Jasa"))
                listCarrier.add(new PekerjaJasa(nama));
            else if (tipe.equalsIgnoreCase("Petugas_Medis"))
                listCarrier.add(new PetugasMedis(nama));
            else if (tipe.equalsIgnoreCase("Cleaning_Service"))
                listCarrier.add(new CleaningService(nama));
            else if (tipe.equalsIgnoreCase("Pegangan_Tangga"))
                listCarrier.add(new PeganganTangga(nama));
            else if (tipe.equalsIgnoreCase("Pintu"))
                listCarrier.add(new Pintu(nama));
            else if (tipe.equalsIgnoreCase("Tombol_Lift"))
                listCarrier.add(new TombolLift(nama));
            else if (tipe.equalsIgnoreCase("Angkutan_Umum"))
                listCarrier.add(new AngkutanUmum(nama));
            else
                ;
        }
        return null;
    }

    public Carrier getCarrier(String nama){
        for (Carrier item: this.listCarrier)
            if (nama.equals(item.getNama()))
                return item;
        return null;
    }
}
