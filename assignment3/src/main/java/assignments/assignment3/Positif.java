package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class Positif implements Status{
  
    @Override
    public String getStatus(){
        return "Positif";
    }

    @Override
    public void tularkan(Carrier penular, Carrier tertular){
        Boolean penularIsHuman = penular.getTipe().equals("Manusia");
        Boolean tertularIsHuman = tertular.getTipe().equals("Manusia");
        int doubleObjectCheck;
        List<Carrier> doubleObject = new ArrayList<>();
        
        if (penularIsHuman == true && tertularIsHuman == true) {
            tertular.ubahStatus("Positif");
            for (Carrier item: penular.getRantaiPenular()) {
                doubleObjectCheck = 0;
                // memastikan tidak ada objek yang sama di dalam rantaiPenular dari penular
                for (Carrier check: penular.getRantaiPenular()) {
                    if (item == check)
                        doubleObjectCheck++;
                }
                if (doubleObjectCheck == 1) {
                    item.tambahTotalKasusDisebabkan();
                    item.tambahAktifKasusDisebabkan();
                } else {
                    if (doubleObject.contains(item))
                        ;
                    else
                        doubleObject.add(item);
                }
                tertular.getRantaiPenular().add(item);
            }
            // Lanjutkan double item
            for (Carrier item: doubleObject) {
                item.tambahTotalKasusDisebabkan();
                item.tambahAktifKasusDisebabkan();
            }
            // Menambah rantai penular, tidak memperhatikan double check
            tertular.getRantaiPenular().add(tertular);
        }
        
        else if (penularIsHuman == true && tertularIsHuman == false) {
            Benda bendaTertular = (Benda) tertular;
            bendaTertular.tambahPersentase();
            
            if (bendaTertular.getPersentaseMenular() >= 100){
                tertular.ubahStatus("Positif");
                for (Carrier item: penular.getRantaiPenular()) {
                    doubleObjectCheck = 0;
                    for (Carrier check: penular.getRantaiPenular()) {
                        if (item == check)
                            doubleObjectCheck++;
                    }
                    if (doubleObjectCheck == 1) {
                        item.tambahTotalKasusDisebabkan();
                    } else {
                        if (doubleObject.contains(item))
                            ;
                        else
                            doubleObject.add(item);
                    }
                    tertular.getRantaiPenular().add(item);
                }
                tertular.getRantaiPenular().add(tertular);
                for (Carrier item: doubleObject) {
                    item.tambahTotalKasusDisebabkan();
                }
            }
        }
        
        else if (penularIsHuman == false && tertularIsHuman == true) {
            tertular.ubahStatus("Positif");
            for (Carrier item: penular.getRantaiPenular()) {
                doubleObjectCheck = 0;
                // memastikan tidak ada objek yang sama di dalam rantaiPenular dari penular
                for (Carrier check: penular.getRantaiPenular()) {
                    if (item == check)
                        doubleObjectCheck++;
                }
                if (doubleObjectCheck == 1) {
                    item.tambahTotalKasusDisebabkan();
                    item.tambahAktifKasusDisebabkan();
                } else {
                    if (doubleObject.contains(item))
                        ;
                    else
                        doubleObject.add(item);
                }
                tertular.getRantaiPenular().add(item);
            }
            // Lanjutkan double item
            for (Carrier item: doubleObject) {
                item.tambahTotalKasusDisebabkan();
                item.tambahAktifKasusDisebabkan();
            }
            // Menambah rantai penular, tidak memperhatikan double check
            tertular.getRantaiPenular().add(tertular);
        }
        
        else { // Penular dan tertular merupakan benda
        }
    }
}