package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        super(nama);
    }

    public void obati(Manusia manusia) {
        manusia.ubahStatus("Negatif");
        this.jumlahDisembuhkan += 1;
        this.tambahSembuh();
        Carrier carrier = (Carrier) manusia;
        List<Carrier> penularCarrierList = carrier.getRantaiPenular();
        int doubleCheck;
        List<Carrier> doubleObject = new ArrayList<>();
        
        for (Carrier item: penularCarrierList) {
            if (item != carrier) {
                doubleCheck = 0;
                for (Carrier check: penularCarrierList) {
                    if (item == check)
                        doubleCheck++;
                }
                if (doubleCheck == 1) {
                    item.kurangiAktifKasusDisebabkan();
                } else {
                    if (doubleObject.contains(item))
                        ;
                    else
                        doubleObject.add(item);
                }
            }
        }
        for (Carrier item: doubleObject)
            item.kurangiAktifKasusDisebabkan();
        // bersihkan rantai penular carrier yang sudah sembuh
        carrier.getRantaiPenular().clear();
    }

    public int getJumlahDisembuhkan(){
        return this.jumlahDisembuhkan;
    }

    @Override
    public String toString() {
        return "PETUGAS MEDIS " + this.getNama();
    }
}