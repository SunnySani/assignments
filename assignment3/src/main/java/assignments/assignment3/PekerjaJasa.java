package assignments.assignment3;

public class PekerjaJasa extends Manusia{
  	
    public PekerjaJasa(String name){
    	super(name);
    }

    @Override
    public String toString() {
        return "PEKERJA JASA " + this.getNama();
    }
  	
}