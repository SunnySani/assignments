package assignments.assignment3;

public class TombolLift extends Benda{
      
    public TombolLift(String name){
        super(name);
    }

    @Override
    public void tambahPersentase() {
        this.setPersentaseMenular(this.persentaseMenular + 20);
    }

    @Override
    public String toString() {
        return "TOMBOL LIFT " + this.getNama();
    }
}