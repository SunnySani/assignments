package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){
        super(nama);
    }

    public void bersihkan(Benda benda){
        benda.setPersentaseMenular(0);
        Carrier carrier = benda;
        carrier.ubahStatus("Negatif");
        carrier.getRantaiPenular().clear();
        jumlahDibersihkan++;
    }

    public int getJumlahDibersihkan(){
        return jumlahDibersihkan;
    }

    @Override
    public String toString() {
        return "CLEANING SERVICE " + this.getNama();
    }

}